(ns com.infinitecloud.omg.application
  (:require
    [cljs.core.async :as async :refer [put! take! <! >! chan sliding-buffer timeout broadcast]])
  (:require-macros [cljs.core.async.macros :as masync :refer [go]]))

(def state (atom {}))

(def incoming-changes (chan (sliding-buffer 1)))
(def outgoing-changes (chan (sliding-buffer 1)))

(defn watch-changes [state]
  (add-watch
    state ::state-change
    (fn [key reference old-state new-state]
      (when-not (= old-state new-state)
        (put! outgoing-changes new-state)))))

(defn ignore-changes [state] (remove-watch state ::state-change))

(go
  (while true
    (let [new-state (<! incoming-changes)]
      (ignore-changes state) ;; we need to ignore changes to avoid round-tripping
      (swap! state (fn [_] new-state))
      (watch-changes state))))

(watch-changes state)