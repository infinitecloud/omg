(ns com.infinitecloud.omg.endpoint
  (:require
    [com.infinitecloud.omg.application :as application]
    [cljs.core.async :as async :refer [put! take! <! >! chan sliding-buffer]]
    [cljs.reader :as reader]
    [clojure.browser.event :as event]
    [clojure.browser.net :as net])
  (:require-macros [cljs.core.async.macros :as masync :refer [go]]))

(def socket (js/WebSocket. (str "ws://" (-> js/window .-document .-location .-host) "/changes")))

(set! (.-onmessage socket)
      (fn [msg]
        (let [new-state (reader/read-string (.-data msg))]
          (println "Receiving " new-state)
          (put! application/incoming-changes new-state))))

(go
  (while true
    (let [new-state (<! application/outgoing-changes)]
      (println "Sending " new-state)
      (.send socket new-state))))