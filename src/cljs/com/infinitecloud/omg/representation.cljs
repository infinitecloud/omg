(ns com.infinitecloud.omg.representation
  (:require
    [com.infinitecloud.omg.application :as application]
    [om.core :as om :include-macros true]
    [om.dom :as dom :include-macros true]
    [sablono.core :as html :refer [html] :include-macros true]))

(enable-console-print!)

(defn render-definition [entity]
  "Returns the render definition for a given entity. The definition
  is determined by looking at the type of the entity and comparing
  it to the type defined as :renders property of the :type."
  (if-let [definitions (filter (fn [[_ data]] (= (:renders data) (:type entity)))
                               (om/join entity []))]
    (second (first definitions))))

(defn coerce [current new]
  "A rather crude approach to coerce the type of the new value to the type
  of the current value. This should be handled by a schema definition."
  (cond
    (number? current) (js/parseFloat new)
    :else new))

(defn render-field [entity owner field-definition]
  "Renders a single field according to its field definition."
  (let [property (:renders field-definition)
        value (if (sequential? property)
                (get-in entity property)
                (get entity property))]
    [:div
     [:label (:label field-definition)]
     [:input
      {:value    (or (om/get-state owner property) value)
       :onChange (fn [event]
                   (om/set-state! owner property (.. event -target -value)))
       :onBlur   (fn [event]
                   (om/transact! entity property
                                 (fn [current]
                                   (coerce current (.. event -target -value)))))}]]))

(defn entity-component [entity owner]
  "Creates a form and renders a field for each property of the entity."
  (if-let [render-definition (render-definition entity)]
    (om/component
      (html [:div
             [:h3 (str "Edit data for " (:first-name entity) " " (:last-name entity))]
             [:form
              (for [field-definition (:field render-definition)]
                (render-field entity owner field-definition))]]))))

(defn application-component [state owner]
  "Returns the application component."
  (om/component
    (html
      [:div
       (if-let [entities (map second (filter
                                       (fn [[_ data]] (= (:type data) :Person))
                                       state))]
         (om/build-all entity-component entities))])))

(om/root application/state application-component (.getElementById js/document "content"))