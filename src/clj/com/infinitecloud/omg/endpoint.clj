(ns com.infinitecloud.omg.endpoint
  (:require
    [com.infinitecloud.omg.application :as application]
    [compojure.core :refer [defroutes GET ANY]]
    [org.httpkit.server :as http]
    [clojure.core.async :as async :refer [go put! take! <! >! chan sliding-buffer mult tap]]
    [compojure.route :as route]
    [ring.util.response :refer [resource-response]]))

(defn socket [request]
  (http/with-channel
    request channel
    (do
      (http/on-receive
        channel
        #(let [new-state (read-string %)]
          (println "Receiving " new-state)
          (put! application/incoming-changes new-state)))
      (go
        (while true
          (let [new-state (pr-str (<! application/outgoing-changes))]
            (println "Sending " new-state)
            (http/send! channel new-state))))
      (http/send! channel (pr-str @application/state)))))

(defroutes
  app
  (GET "/changes" [] socket)
  (GET "/" [] (resource-response "index.html" {:root "public"}))
  (route/resources "/"))

(defn -main [& args]
  (http/run-server #'app {:port 8080}))

(reset! application/state
        {:form0   {:type    :Form
                   :label   "Person"
                   :renders :Person
                   :field   [{:type    :TextField
                              :renders :first-name
                              :label   "First Name"}
                             {:type    :TextField
                              :renders :last-name
                              :label   "Last Name"}
                             {:type    :TextField
                              :renders :age
                              :label   "Age"}]}
         :person0 {:type       :Person
                   :first-name "Jochen"
                   :last-name  "Rau"
                   :age        23.54}
         :person1 {:type       :Person
                   :first-name "David"
                   :last-name  "Nolen"}})