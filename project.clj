(defproject com.infinitecloud/omg "0.1.0-SNAPSHOT"
  :description "An example for the Om library (https://github.com/swannodette/om)"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src/clj"]
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-2138"]
                 [org.clojure/core.async "0.1.267.0-0d7780-alpha"]
                 [http-kit "2.1.10"]
                 [ring "1.2.0"]
                 [compojure "1.1.5"]
                 [om "0.3.0"]
                 [sablono "0.2.1"]]
  :cljsbuild {:builds [{:id           "dev"
                        :source-paths ["src/cljs"]
                        :compiler     {:output-to     "resources/public/js/application.js"
                                       :output-dir    "resources/public/js/out"
                                       :optimizations :whitespace
                                       :pretty-print  true}}]}
  :repl-options {:port 4001}
  :main com.infinitecloud.omg.endpoint)
