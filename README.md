# Om Example Project

This is an example web-application to demo the capabilities of [Om](https://github.com/swannodette/om). Om is a Clojure library on top of Facebook's [React](facebook.github.io/react).

## Run it

The application is self-contained. You just need to have [Leiningen 2](https://github.com/technomancy/leiningen) installed to compile and run it. First, clone the repository

	git clone https://bitbucket.org/infinitecloud/omg.git
	cd omg

Then start the server with `lein run` and point your browser to [http://localhost:8080](http://localhost:8080).

To interact with the code, start a repl with `lein repl` and run the server with `(-main)`. In a new terminal start `lein cljscompile auto` to continuously compile changes made to the ClojureScript.

## Architecture

The application state is synchronized between browser and server through a [core.async](https://github.com/clojure/core.async) channels and a WebSocket (implemented in `com.infinitecloud.omg.application` and `com.infinitecloud.omg.endpoint` on both sides). The state on both sides is watched and every change is sent to the other side. The representation automatically gets updated, too. And that is where Om really shines. Om makes data binding very transparent and offers nice semantics to keep local component state and application state separated. Here is the crucial snippet:

	(ns com.infinitecloud.omg.representation
	  (:require
		[com.infinitecloud.omg.application :as application]
		[om.core :as om :include-macros true]
		[om.dom :as dom :include-macros true]
		[sablono.core :as html :refer [html] :include-macros true]))

	[...]

	(defn render-field [entity owner field-definition]
	  "Renders a single field according to its field definition."
	  (let [property (:renders field-definition)
			value (if (sequential? property)
					(get-in entity property)
					(get entity property))]
		[:div
		 [:label (:label field-definition)]
		 [:input
		  {:value    (or (om/get-state owner property) value)
		   :onChange (fn [event]
					   (om/set-state! owner property (.. event -target -value)))
		   :onBlur   (fn [event]
					   (om/transact! entity property
									 (fn [current]
									   (coerce current (.. event -target -value)))))}]]))

	(defn application-component [state owner]
	  "Returns the application component."
	  (om/component
		(html
		  [:div
		   (if-let [entities (map second (filter
										   (fn [[_ data]] (= (:type data) :Person))
										   state))]
			 (om/build-all entity-component entities))])))

	(om/root application/state application-component (.getElementById js/document "content"))



## License

Copyright © 2014 Jochen Rau, Infinite Cloud LLC

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
